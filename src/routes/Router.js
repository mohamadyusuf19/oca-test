import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "../pages/Home";
import Login from "../pages/Login";
import Scheduler from "../pages/Scheduler";

export default function Routes() {
  return (
    <Router>
      <Switch>
        <Route path='/login' component={Login} />
        <Route exact path='/' component={Home} />
        <Route path='/scheduler' component={Scheduler} />
      </Switch>
    </Router>
  );
}
