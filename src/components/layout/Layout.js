import React, { useState } from "react";
import Navbar from "../navbar/Navbar";
import Sidebar from "../sidebar/Sidebar";

const Layout = ({ children }) => {
  const [activeSidebar, setActiveSidebar] = useState(false);
  const onDisplaySidebar = () => {
    setActiveSidebar(true);
  };
  const onCloseSidebar = () => {
    setActiveSidebar(false);
  };
  return (
    <div className='flex'>
      <Sidebar onCloseSidebar={onCloseSidebar} activeSidebar={activeSidebar} />
      <div className='layout-main md:w-4/5 w-full'>
        <div className='md:p-16 p-8'>
          <Navbar onDisplaySidebar={onDisplaySidebar} />
          {children}
        </div>
      </div>
    </div>
  );
};

export default Layout;
