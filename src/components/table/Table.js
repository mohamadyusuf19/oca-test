import React from "react";
import sortIcon from "../../assets/sort.svg";
import filterIcon from "../../assets/filter.svg";

const Table = ({ data }) => {
  return (
    <div className='border border-gray-300 rounded px-8 '>
      <div className='border-b border-gray-300 flex items-center justify-between py-3 mb-2'>
        <p className='text-basic font-bold text-sm'>Recent BLAST</p>
        <div className='flex items-center'>
          <div className='flex items-center mx-1 cursor-pointer'>
            <img src={sortIcon} alt='sortIcon' className='mx-1' />
            <p className='text-xs text-gray-600'>Sort</p>
          </div>
          <div className='flex items-center mx-1  cursor-pointer'>
            <img src={filterIcon} alt='filterIcon' className='mx-1' />
            <p className='text-xs text-gray-600'>Filter</p>
          </div>
        </div>
      </div>
      <div className='h-40 w-full overflow-auto scroll'>
        <table>
          <thead>
            <tr>
              <th>Id Name</th>
              <th>Type</th>
              <th>Campaign</th>
              <th>Total BLast</th>
              <th>Status</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody style={{ textAlign: "center", height: 22 }}>
            {data.map((item, i) => {
              return (
                <tr key={i}>
                  <td className='text-left w-48'>{item.idName}</td>
                  <td>{item.type}</td>
                  <td>{item.campaign}</td>
                  <td>{item.totalBlast}</td>
                  <td className={item.status}>{item.status}</td>
                  <td className='text-left'>{item.date}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Table;
