import React, { useState } from "react";
import ocaLogo from "../../assets/OCA-logo.svg";
import homeIcon from "../../assets/homeIcon.svg";
import chatIcon from "../../assets/chatIcon.svg";
import dropdownIcon from "../../assets/dropdown.svg";
import rectangleIcon from "../../assets/Rectangle.svg";
import { MdClear } from "react-icons/md";
import { useHistory } from "react-router";
const Sidebar = ({ onCloseSidebar, activeSidebar }) => {
  const history = useHistory();
  const [activeDropdown, setaAtiveDropdown] = useState(true);
  const handleDropdown = () => {
    setaAtiveDropdown((prevState) => !prevState);
  };

  const onRoutesSms = () => {
    history.push("/scheduler");
    onCloseSidebar();
  };

  const onRoutesHome = () => {
    history.push("/");
    onCloseSidebar();
  };

  return (
    <div
      className={`md:block fixed md:w-1/5 w-3/5 bg-basic h-screen ${
        activeSidebar ? "absolute" : "hidden"
      }`}>
      <MdClear
        className='md:hidden block text-white text-2xl m-3 cursor-pointer'
        onClick={onCloseSidebar}
      />
      <img src={ocaLogo} alt='oca-logo' className='mx-auto md:mt-20 mt-4' />
      <div className='md:mt-64 px-8 mt-20'>
        <div
          className='flex items-center mb-3 cursor-pointer'
          onClick={onRoutesHome}>
          <img src={homeIcon} alt='homeIcon' className='mr-2' />
          <p className='text-white'>Home</p>
        </div>
        <div
          className='flex items-center justify-between cursor-pointer'
          onClick={handleDropdown}>
          <div className='flex items-center'>
            <img src={chatIcon} alt='chatIcon' className='mr-2' />
            <p className='text-white'>SMS</p>
          </div>
          <div>
            <img
              src={dropdownIcon}
              alt='chatIcon'
              className={`transform ${activeDropdown && "rotate-180"}`}
            />
          </div>
        </div>
        {!activeDropdown && (
          <div>
            <div className='flex items-end my-3'>
              <img src={rectangleIcon} alt='rectangleIcon' className='mr-2' />
              <p className='text-white -mb-3 cursor-pointer'>Broadcast</p>
            </div>
            <div className='flex items-end my-3'>
              <img src={rectangleIcon} alt='rectangleIcon' className='mr-2' />
              <p
                className='text-white -mb-3 cursor-pointer'
                onClick={onRoutesSms}>
                SMS Scheduller
              </p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Sidebar;
