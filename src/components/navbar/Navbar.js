import React from "react";
import { MdSearch, MdAccountCircle, MdMenu } from "react-icons/md";
const Navbar = ({ onDisplaySidebar }) => {
  return (
    <React.Fragment>
      <div className='md:flex hidden items-center justify-between'>
        <div className='border w-2/5 h-10 flex items-center px-2'>
          <MdSearch className='text-gray-500 text-xl' />
          <input type='text' className='ml-2' placeholder='Any help?' />
        </div>
        <div className='flex items-center'>
          <div className='text-right'>
            <p className='text-base font-bold'>Hi, Adjie!</p>
            <p className='text-xs text-gray-700'>Adjie_g4ant3ng@banget.com</p>
          </div>
          <MdAccountCircle className='text-5xl text-gray-700 ml-3' />
        </div>
      </div>
      <div className='md:hidden flex items-center justify-between'>
        <MdMenu
          className='text-basic text-4xl cursor-pointer'
          onClick={onDisplaySidebar}
        />
        <div className='flex items-center'>
          <div className='text-right'>
            <p className='text-base font-bold'>Hi, Adjie!</p>
            <p className='text-xs text-gray-700'>Adjie_g4ant3ng@banget.com</p>
          </div>
          <MdAccountCircle className='text-5xl text-gray-700 ml-3' />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Navbar;
