import React from "react";

const InputCustom = ({ title, placeholder, children }) => {
  return (
    <div className='mb-3 mt-5'>
      <p className='text-black mb-2 text-sm'>{title}</p>
      <div className='flex items-center justify-between border border-gray-300 md:w-1/4 w-full h-10 rounded px-2'>
        <input type='text' placeholder={placeholder} />
        <div className='flex items-center'>{children}</div>
      </div>
    </div>
  );
};

export default InputCustom;
