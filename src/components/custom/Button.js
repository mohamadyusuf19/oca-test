import React from "react";

const Button = ({ onClick, text, style }) => {
  return (
    <div
      className='bg-basic h-10 cursor-pointer flex items-center justify-center rounded hover:bg-red-700'
      style={style}
      onClick={onClick}>
      <p className='text-white '>{text}</p>
    </div>
  );
};

export default Button;
