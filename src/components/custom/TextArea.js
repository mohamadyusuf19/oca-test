import React, { useState } from "react";

const TextareaCustom = ({ title }) => {
  const [text, setText] = useState(
    `Halo Bili, perkenalkan kami OCA (Omni Communication Assistant). Aplikasi berbasis web yang bisa membantu kamu menyebarkan pesan baik itu menggunakan voice ataupun text. Kamu bisa menjangkau seluruh konsumen dengan cepat, masif, dan otomatis. Info lebih lanjut kunjungi web kami 
      
www.ocatelkom.co.id`
  );
  return (
    <div className='mb-3 mt-5'>
      <p className='text-black mb-2 text-sm'>{title}</p>
      <div className='md:w-3/5 w-full border-b-2 border-gray-300 pb-3 mb-5'>
        <textarea
          className='flex items-center justify-between border border-gray-300 w-full h-48 rounded p-5'
          rows={4}
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <p className='text-sm text-gray-600 text-right mt-2'>
          Character {text.length}/1024
        </p>
      </div>
    </div>
  );
};

export default TextareaCustom;
