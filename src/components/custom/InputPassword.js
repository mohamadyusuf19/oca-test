import React from "react";

const InputPassword = () => {
  return (
    <div className='mb-10'>
      <p className='text-gray-400 mb-2 text-sm'>Password</p>
      <div className='flex items-center justify-between border border-gray-300 w-full h-10 rounded px-2'>
        <input type='password' className='' />
        <div className='flex items-center'>
          <p className='text-gray-400'>|</p>
          <p className='text-gray-400 cursor-pointer hover:text-gray-700 ml-2 text-sm'>
            Forgot?
          </p>
        </div>
      </div>
    </div>
  );
};

export default InputPassword;
