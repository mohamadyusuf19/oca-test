import React from "react";

const Input = ({ title, placeholder }) => {
  return (
    <div className='mb-3'>
      <p className='text-gray-400 mb-2 text-sm'>{title}</p>
      <input
        type='text'
        className='border border-gray-300 w-full h-10 rounded px-2'
        placeholder={placeholder}
      />
    </div>
  );
};

export default Input;
