import React from "react";

const RadioButton = ({ title, checked }) => {
  return (
    <label className='container-radio'>
      {title}
      <input type='radio' />
      <span className='checkmark-radio' checked={checked}></span>
    </label>
  );
};

export default RadioButton;
