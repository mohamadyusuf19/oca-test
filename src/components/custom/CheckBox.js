import React from "react";

const CheckBox = ({ week, onChecked }) => {
  return (
    <React.Fragment>
      {week.map((item, i) => (
        <label className='container' key={i}>
          {item.day}
          <input
            type='checkbox'
            checked={item.checked}
            onChange={() => onChecked(i)}
          />
          <span className='checkmark'></span>
        </label>
      ))}
    </React.Fragment>
  );
};

export default CheckBox;
