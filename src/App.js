import { Provider } from "jotai";
import React from "react";
import Routes from "./routes/Router";

const App = () => {
  return (
    <Provider>
      <Routes />
    </Provider>
  );
};

export default App;
