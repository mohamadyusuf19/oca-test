export const data = [
  {
    idName: "MTN_EGY-LIFE",
    type: "Call",
    campaign: "Reminding Billing 20 September",
    totalBlast: "1255",
    status: "Success",
    date: "Jul 15, 2018 4:47 AM",
  },
  {
    idName: "MTN_BCA",
    type: "Call",
    campaign: "Reminding Billing 20 September",
    totalBlast: "600",
    status: "Failed",
    date: "August 3, 2012 5:26 PM",
  },
  {
    idName: "RTL_1-BPNA",
    type: "SMS",
    campaign: "Pengingat Pagi",
    totalBlast: "121223",
    status: "Pending",
    date: "October 12, 2018 1:47 AM",
  },
  {
    idName: "KUI_ACE",
    type: "Email",
    campaign: "Test",
    totalBlast: "958433",
    status: "Progress",
    date: "March 13, 2016 2:25 AM",
  },
  {
    idName: "MTN_EGY-LIFE",
    type: "Email",
    campaign: "Reminding Billing 20 September",
    totalBlast: "1255",
    status: "Failed",
    date: "Jul 15, 2018 4:47 AM",
  },
];
