import React, { useState } from "react";
import InputCustom from "../components/custom/InputCustom";
import Layout from "../components/layout/Layout";
import dropdownIcon from "../assets/dropdownPink.svg";
import TextareaCustom from "../components/custom/TextArea";
import CheckBox from "../components/custom/CheckBox";
import RadioButton from "../components/custom/RadioButton";
import Button from "../components/custom/Button";

const Scheduler = () => {
  const [time, setTime] = useState("02:00");
  const [week, setWeek] = useState([
    { day: "Mon", checked: true },
    { day: "Tue", checked: true },
    { day: "Wed", checked: true },
    { day: "Thu", checked: false },
    { day: "Fri", checked: false },
    { day: "Sat", checked: true },
    { day: "Sun", checked: true },
  ]);
  const onChangeDay = (day) => {
    const data = week.filter((item, i) => {
      if (day === i) {
        item.checked = !item.checked;
        return item;
      } else {
        return item;
      }
    });
    setWeek(data);
  };
  return (
    <Layout>
      <div className='mt-10'>
        <p className='font-bold text-4xl'>SMS Scheduller</p>
        <InputCustom
          title='Broadcast Name'
          placeholder='name of your broadcast'
        />
        <InputCustom title='Phonebook' placeholder='select your phonebook'>
          <img src={dropdownIcon} alt='dropdown' className='cursor-pointer' />
        </InputCustom>
        <TextareaCustom title='Message' />
        <p className='text-black mb-2'>Schedule</p>
        <p className='text-gray-600 mb-3'>
          When and how often do you want to broadcast this messages?
        </p>
        <div className='md:w-3/5 w-full grid md:grid-cols-4 grid-cols-1'>
          <div className='col-span-1'>
            <p className='text-black mb-3'>Run On</p>
          </div>
          <div className='col-span-3'>
            <div className='grid md:grid-cols-7 grid-cols-4'>
              <CheckBox week={week} onChecked={onChangeDay} />
            </div>
            <div className='grid md:grid-cols-4 grid-cols-1 mb-2'>
              <RadioButton title='Once a day' checked={true} />
              <div className='flex items-center mt-2'>
                <p>At</p>
                <input
                  type='time'
                  className='mx-2 border border-gray-500 rounded pl-1'
                  value={time}
                  onChange={(e) => setTime(e.target.value)}
                />
                <p>WIB</p>
              </div>
            </div>
            <div className='grid md:grid-cols-4 grid-cols-1 mb-10'>
              <div className='col-span-1'>
                <RadioButton title='At Intervals' />
              </div>
              <div className='col-span-3'>
                <div className='flex items-center'>
                  <p>Every</p>
                  <input
                    type='number'
                    disabled
                    value={6}
                    className='w-12 mx-2 cursor-not-allowed'
                  />
                  <input
                    type='number'
                    disabled
                    placeholder='hours'
                    className='w-16 mx-1 cursor-not-allowed'
                  />
                  <p>From</p>
                  <input
                    type='time'
                    className='mx-2 border border-gray-500 rounded pl-1 cursor-not-allowed'
                    value={time}
                    disabled
                  />
                  <p>WIB</p>
                </div>
              </div>
            </div>
            <div className='float-right'>
              <Button text='Send messages' style={{ width: 200 }} />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Scheduler;
