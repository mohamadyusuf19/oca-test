import React from "react";
import primaryBg from "../assets/primary-backround.svg";
import secondBg from "../assets/nineIcon.svg";
import ocaLogo from "../assets/logo.svg";
import Input from "../components/custom/Input";
import InputPassword from "../components/custom/InputPassword";
import Button from "../components/custom/Button";
import { useHistory } from "react-router";
import { isAuthenticatedReducer } from "../jotai/auth";
import { useAtom } from "jotai";

const Login = () => {
  const history = useHistory();
  const [, setIsAuthenticated] = useAtom(isAuthenticatedReducer);
  const onSubmit = () => {
    history.push("/");
    setIsAuthenticated(true);
  };
  return (
    <div className='flex '>
      <div className='md:w-2/5 w-full flex items-center justify-center pt-20 md:pt-0'>
        <div>
          <img src={ocaLogo} alt='oca-logo' className='mb-12' />
          <p className='my-3 shadow-2xl'>
            Welcome Back, Please login into your account
          </p>
          <Input title='Username / Email' />
          <InputPassword />
          <Button text='sign in' onClick={onSubmit} />
        </div>
      </div>
      <div className='md:w-3/5 md:block hidden'>
        <div
          className='bg-basic h-screen w-full flex items-center justify-center'
          style={{
            backgroundImage: `url("${primaryBg}")`,
            backgroundSize: "cover",
          }}>
          <img
            src={secondBg}
            alt='background'
            className=' h-screen w-full object-cover'
          />
        </div>
      </div>
    </div>
  );
};

export default Login;
