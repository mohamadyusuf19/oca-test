import { useAtom } from "jotai";
import React, { useEffect } from "react";
import { useHistory } from "react-router";
import Layout from "../components/layout/Layout";
import Table from "../components/table/Table";
import { data } from "../data/data";
import { isAuthenticatedReducer } from "../jotai/auth";

const Home = () => {
  const history = useHistory();
  const [isAuthenticated] = useAtom(isAuthenticatedReducer);
  useEffect(() => {
    if (!isAuthenticated) {
      history.push("/login");
    }
  }, []);
  const QuotaComponent = ({ title, detail, classNameDetail }) => {
    return (
      <div className='w-full border border-gray-300 rounded flex items-center justify-between p-3'>
        <p>{title}</p>
        <p className={`font-bold text-xl ${classNameDetail}`}>{detail}</p>
      </div>
    );
  };
  return (
    <Layout>
      <div className='rounded border border-gray-300 w-full mt-20'>
        <div className='bg-basic h-2 rounded-t-full' />
        <div className='p-8 '>
          <p className='text-4xl font-bold'>Hello, Welcome to OCA!</p>
          <p>
            Let’s start make some noise and make your campaign great again!
            click here to spread your messages.
          </p>
        </div>
      </div>
      <div className='mt-16'>
        <p className='my-3 font-bold text-gray-500'>Remaining Quota</p>
        <div className='grid md:grid-cols-4 grid-cols-1 md:gap-8 gap-2'>
          <QuotaComponent
            title='Call'
            detail='4840 seconds'
            classNameDetail='text-blue-400'
          />
          <QuotaComponent
            title='SMS'
            detail='1245 Messages'
            classNameDetail='text-yellow-400'
          />
          <QuotaComponent
            title='Email'
            detail='7710 mails'
            classNameDetail='text-purple-400'
          />
          <QuotaComponent
            title='Whatsapp'
            detail='330 messages'
            classNameDetail='text-green-400'
          />
        </div>
        <div className='mt-16'>
          <Table data={data} />
        </div>
      </div>
    </Layout>
  );
};

export default Home;
